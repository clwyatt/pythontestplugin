# parse properties file
import os, sys, shutil
propfile = sys.argv[1]

import jprops
with open(propfile) as fp:
  properties = jprops.load_properties(fp)

workingDir = properties['workingDir']
resultDir = properties['resultDir']
pluginData = properties['pluginData']
scriptData = properties['scriptData']
localFiles = properties['localFiles']

reportCount = int(properties['numReports'])

# Copy testing files from localFiles to working dir
localPath = os.path.join(scriptData, localFiles)
if os.path.isdir(localPath):
	src_files = os.listdir(localPath)
	for file_name in src_files:
		full_file_name = os.path.join(localPath, file_name)
		if os.path.isfile(full_file_name):
			shutil.copy(full_file_name, workingDir)
if os.path.isfile(localPath):
	shutil.copyfile(localPath,
					os.path.join(workingDir, os.path.basename(localFiles)))

# cd to working directory and run nose producing xunit xml file
os.chdir(workingDir)

import nose, plugin_utils

argv = [""]
argv.insert(1, "--with-xunit")
argv.insert(1, "--nocapture")

devnull = open(os.devnull, 'w')
with plugin_utils.RedirectStdStreams(stdout=devnull, stderr=devnull):
	nose.run(argv=argv)

shutil.copyfile("nosetests.xml",os.path.join(resultDir,"nosetests.xml"))

# now parse the xunit xml file
import xunitparser
ts, tr = xunitparser.parse(open('nosetests.xml'))

# compute a score and write feedback file
total_tests = 0
passed_tests = 0
feedbackfile = os.path.join(resultDir, "feedback.html")
with open(feedbackfile, 'w') as fp:
	fp.write('<div class="module">\n')
	fp.write('<div dojoType="webcat.TitlePane" title="Test Summary" open="true">\n')
	for tc in ts:
		fp.write('<p>Test %s in %s: ' % (tc.methodname, tc.classname))
		total_tests += 1
		if tc.good:
			if tc.skipped:
				fp.write('<b>Skipped.</b></p>\n')
			else:
				fp.write('<b>Passed.</b></p>\n')
				passed_tests += 1
		else:
			fp.write('<b class="warn">Failed</b>!</p>\n')
	fp.write('<br>\n')
	fp.write('<p>Tests Passed / Total : %i / %i</p>\n' % (passed_tests, total_tests))
	fp.write("</div></div>\n")
	reportCount += 1

# write to properties file
properties['score.correctness'] = str(passed_tests)
properties['numReports'] = str(reportCount)
properties['report1.file'] = 'feedback.html'
properties['report1.mimeType'] = 'text/html'
properties['report1.styleVersion'] = '1'

with open(propfile, 'w') as fp:
	jprops.store_properties(fp, properties)
