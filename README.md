Introduction
============

PythonTestPlugin is a plugin for the [Web-CAT](http://wiki.web-cat.org/WCWiki/WhatIsWebCat) automated grading system that supports testing python code using Python [nose](https://pypi.python.org/pypi/nose/), producing a score based on the number of passing tests and providing a summary of passing and failing tests.

Dependencies:
* Python 2.7.6
* nose 1.3.0
* jprops - for reading/writing java peroperties files
* xunitparser - for parsing nose output
* Java - for creating the jar file only (almost any version will do)

Installation
============
The following assumes you have access to a running Web-CAT instance. See the development section below for how to create one yourself.

1. Create a jar file containing the plist and python files, or use the provided Makefile.
2. Login to Web-CAT and navigate to the Plugins page under Assignments in the staff role.
3. Choose manually upload a plugin and upload the jar file produced in step 1.

You are now ready to create an assignment using the plugin.

Example Assignment
==================

Instructor:

Student:

Development
===========

TODO
====
* add captured output from failing tests to report
* add coverage run and report, see http://nedbatchelder.com/code/coverage/
