FILES = config.plist execute.py jprops.py plugin_utils.py xunitparser.py

all:
	jar cf PythonTestPlugin.jar $(FILES)

clean:
	$(RM)  PythonTestPlugin.jar
