#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get -y install curl
apt-get -y install tomcat7
apt-get -y install default-jdk

MYSQL_PASSWORD="mysql123"

echo "mysql-server-5.5 mysql-server/root_password password $MYSQL_PASSWORD" | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections
apt-get -y install mysql-server-5.5

# deploy Web-CAT 1.4
curl -L -o /var/lib/tomcat7/webapps/Web-CAT.war http://sourceforge.net/projects/web-cat/files/Web-CAT%20Servlet/1.4.0/Web-CAT_1.4.0.war/download
service tomcat7 restart

# make webcat data directory
mkdir /home/tomcat7
sudo chown tomcat7:tomcat7 /home/tomcat7

# setup python environment
apt-get -y install python-pip
pip install nose
